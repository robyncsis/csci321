package paystation.domain;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
	@Suite.SuiteClasses(
			{TestDanishPaymentStrategy.class, TestDanishRate.class, TestIntegration.class, 
				TestLinearRate.class, TestPayStation.class, TestProgressiveRate.class, TestUSPaymentStrategy.class}
			)



public class TestAll { //Dummy

}
