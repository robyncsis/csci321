package paystation.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestDanishRate {

	  /** Test a single hour parking */
	  @Test 
	  public void shouldDisplay21MinFor3Kroner() {
	    RateStrategy rs = new DanishRateStrategy();
	    assertEquals( 3*7, rs.calculateTime(3) ); 
	  }

}
