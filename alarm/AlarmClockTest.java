import static org.junit.Assert.*;

import org.junit.Test;

public class AlarmClockTest {

	@Test
	public void shouldHandleAllBasics() {
		int[] clock = {11, 30};
		int[] alarm = {8, 30};
		AlarmClock ac = new AlarmClockImpl(clock, alarm);
		assertEquals("11:30", ac.readDisplay());
		ac.increase();
		assertEquals("11:30", ac.readDisplay());
		ac.decrease();
		assertEquals("11:30", ac.readDisplay());
		ac.mode();
		assertEquals("8:30", ac.readDisplay());
		
		ac.increase();
		ac.increase();
		assertEquals("10:30", ac.readDisplay());
		ac.decrease();
		ac.decrease();
		ac.decrease();
		ac.decrease();
		assertEquals("6:30", ac.readDisplay());
		ac.mode();
		assertEquals("6:30", ac.readDisplay());
		
		ac.increase();
		ac.increase();
		assertEquals("6:32", ac.readDisplay());
		ac.decrease();
		ac.decrease();
		ac.decrease();
		ac.decrease();
		assertEquals("6:28", ac.readDisplay());
		ac.mode();
		

		assertEquals("11:30", ac.readDisplay());
		ac.mode();
		assertEquals("6:28", ac.readDisplay());
	}
	
	@Test
	public void shouldHandleTurnover() {
		int[] clock = {11, 30};
		int[] alarm = {12, 59};
		AlarmClock ac = new AlarmClockImpl(clock, alarm);
		
		ac.mode();
		ac.increase();
		assertEquals("1:59", ac.readDisplay());
		ac.decrease();
		assertEquals("12:59", ac.readDisplay());
		
		ac.mode();
		ac.increase();
		assertEquals("1:00", ac.readDisplay());
		ac.decrease();
		assertEquals("12:59", ac.readDisplay());
	}
}
