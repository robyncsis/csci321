
public class AlarmClockImpl implements AlarmClock {
    AlarmClock 
    	clockState,
    	alarmState,
    	state;
  
    public AlarmClockImpl(int[] clock, int[] alarm) {
    	clockState = new ClockState(this, clock);
    	alarmState = new AlarmState(this, alarm);
    	state = clockState;
    }
    
	@Override
	public String readDisplay() {
		return state.readDisplay();
	}

	@Override
	public void mode() {
		state.mode();
	}

	@Override
	public void increase() {
		state.increase();
	}

	@Override
	public void decrease() {
		state.decrease();
	}
	
	
	
	public static void main(String[] args) {
		int[] clock = {11, 30};
		int[] alarm = {8, 30};
		AlarmClock ac = new AlarmClockImpl(clock, alarm);
		System.out.println("Alarmclock demo");
		System.out.println(ac.readDisplay());
		ac.mode();
		System.out.println("Change mode to alarm hours: " + ac.readDisplay());
		ac.increase();
		ac.increase();
		System.out.println("Increase hours twice: " + ac.readDisplay());
		ac.decrease();
		ac.decrease();
		ac.decrease();
		ac.decrease();
		System.out.println("Decrease hours four times: " + ac.readDisplay());
		System.out.println("Change mode to alarm minutes");
		ac.mode();
		ac.increase();
		ac.increase();
		System.out.println("Increase alarm minutes twice: " + ac.readDisplay());
		ac.decrease();
		System.out.println("Decrease alarm minutes once just to leave it on a \n\tweird time and keep me on my toes: " + ac.readDisplay());

		int[] alarm2 = {12, 59};
		ac = new AlarmClockImpl(clock, alarm2);
		System.out.println("\nAlarmclock rollover demo (mode set to alarm)");
		ac.mode();
		System.out.println(ac.readDisplay());
		ac.increase();
		System.out.println("Increase hours once:" + ac.readDisplay());
		ac.decrease();
		System.out.println("Decrease hours once:" + ac.readDisplay());
		ac.mode();
		ac.increase();
		System.out.println("Increase minutes once:" + ac.readDisplay());
		ac.decrease();
		System.out.println("Decrease minutes once:" + ac.readDisplay());
	}

}

	class ClockState implements AlarmClock {
		int[] clock;
		AlarmClock alarmClock;
		public ClockState(AlarmClock ac, int[] clock) { 
			this.alarmClock = ac; 
			this.clock = clock;
		}
		@Override
		public String readDisplay() {
			if(clock[1] < 10) return clock[0] + ":0" + clock[1];
			else return clock[0] + ":" + clock[1];
		}
		@Override
		public void mode() {
			((AlarmClockImpl)alarmClock).state = ((AlarmClockImpl)alarmClock).alarmState;
		}
		
		/**
		 * These two aren't used by clock state so it violates SOLID?
		 */
		@Override
		public void increase() {
			// For some reason the exercise doesn't want people to be able 
			// to set the clock time so this is left blank
		}
		@Override
		public void decrease() {
			// For some reason the exercise doesn't want people to be able 
			// to set the clock time so this is left blank
		}
	}
	
	class AlarmState implements AlarmClock{
		static byte HOURS = 0,
				   MINS = 1;
		int[] alarm;
		byte state;
		AlarmClock alarmClock;
		public AlarmState(AlarmClock ac, int[] alarm) {
			this.alarm = alarm;
			alarmClock = ac;
			state = HOURS;
		}

		@Override
		public String readDisplay() {
			if(alarm[MINS] < 10) return alarm[HOURS] + ":0" + alarm[MINS];
			else return alarm[HOURS] + ":" + alarm[MINS];
		}

		@Override
		public void mode() {
			if(state == HOURS) 	state = MINS;
			else {
				state = HOURS;
				((AlarmClockImpl)alarmClock).state = ((AlarmClockImpl)alarmClock).clockState;
			}
		}

		@Override
		public void increase() {
			if(state == HOURS) increaseHours();
			else increaseMins();
		}

		@Override
		public void decrease() {
			if(state == HOURS) decreaseHours();
			else decreaseMins();
		}
		
		private void increaseHours(){
			if(alarm[HOURS] == 12) alarm[HOURS] = 1;
			else alarm[HOURS]++;
		}
		private void increaseMins(){
			if(alarm[MINS] == 59) {
				alarm[MINS] = 0;
				increaseHours();
			}else {
				alarm[MINS]++;
			}
		}
		private void decreaseHours(){
			if(alarm[HOURS] == 1) {
				alarm[HOURS] = 12;
			}else {
				alarm[HOURS]--;
			}
		}
		private void decreaseMins(){
			if(alarm[MINS] == 0) {
				alarm[MINS] = 59;
				decreaseHours();
			}else {
				alarm[MINS]--;
			}
		}	
	}