
public class OmegaDisplayImpl implements DisplayStrategy{
	int hours = 0;
	int mins = 0;
	@Override
	public int readDisplay(int timeBought) {
		figureTime(timeBought);
		String time = hours + formatTime(mins);
		int formattedTime = Integer.parseInt(time, 10);
		return formattedTime;
	}
	
	public void setTime(int hours, int mins) {
		this.hours = hours;
		this.mins = mins;
	}
	
	private void figureTime(int timeBought) {
		mins = mins+timeBought;
		while(mins >=60) {
			if(hours < 23)
				hours++;
			else
				hours = 0;
			mins-=60;
		}
	}
	
	private String formatTime(int value) {
		String string = "0";
		if(value < 10) {
			string = string + value;
		}else {
			string = "" + value;
		}
		return string;
		
		
		
//		String hoursString = "0";
//		if(hours < 10) {
//			minString = minString+mins;
//		}else {
//			minString = ""+mins;
//		}
//		String minString = "0";
//		if(mins < 10) {
//			minString = minString+mins;
//		}else {
//			minString = ""+mins;
//		}
			
	}

}
