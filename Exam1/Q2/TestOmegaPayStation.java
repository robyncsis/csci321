import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestOmegaPayStation {
	PayStation ps;
	 /** Fixture for pay station testing. */
	 @Before
	 public void setUp() {
	   ps = new PayStationImpl(new OmegaDisplayImpl());
	 }
	
	 /**
	  * Test that the PayStation displays the right time 
	  * for 5 cents without rolling over the minutes
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay1832For5Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(18, 30);
		ps.addPayment( 5 );
	    assertEquals( "Should display 1832 for 5 cents",
	                  1832, ps.readDisplay() );
	 }
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 10 cents without rolling over the minutes
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay1848For25Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(18, 30);
		ps.addPayment(25);
	    assertEquals( "Should display 1840 for 25 cents",
	                  1840, ps.readDisplay() );
	 }
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 35 cents without rolling over the minutes
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay1844For35Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(18, 30);
		ps.addPayment(10);
		ps.addPayment(25);
	    assertEquals( "Should display 1844 for 10 and 25 cents",
	                  1844, ps.readDisplay() );
	 }
	 
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 5 cents just before rolling over minutes
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay1659For5Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(18, 57);
		ps.addPayment(5);
	    assertEquals( "Should display 1859 for 5 cents",
	                  1859, ps.readDisplay() );
	 }
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 75 cents while rolling over the minutes, incrementing hours
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay1700For75Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(18, 30);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
	    assertEquals( "Should display 1900 for 75 cents",
	                  1900, ps.readDisplay() );
	 }
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 5 cents while rolling over the minutes +1, incrementing hours
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay1701For5Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(18, 59);
		ps.addPayment(5);
	    assertEquals( "Should display 1901 for 5 cents",
	                  1901, ps.readDisplay() );
	 }
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 5 cents just before running over the minutes AND hours
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay2359For5Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(23, 57);
		ps.addPayment(5);
	    assertEquals( "Should display 2359 for 5 cents",
	                  2359, ps.readDisplay() );
	 }
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 5 cents while rolling over the minutes AND hours
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay0000For5Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(23, 58);
		ps.addPayment(5);
	    assertEquals( "Should display 0000 for 5 cents",
	                  0000, ps.readDisplay() ); //Really it displays 0 bc the directions seemed to ask 
	    										//for an int to be displayed
	 }
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 5 cents while rolling over the minutes +1 AND hours
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay0001For5Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(23, 59);
		ps.addPayment(5);
	    assertEquals( "Should display 0001 for 5 cents",
	    		0001, ps.readDisplay()); //Really it displays 1 bc the directions seemed to ask 
		//for an int to be displayed
	 }
	 
	 /**
	  * Test that the PayStation displays the right time 
	  * for 5 cents while rolling over the minutes +1 AND hours +1
	  * @throws IllegalCoinException
	  */
	 @Test
	 public void shouldDisplay0101For155Cents() throws IllegalCoinException {
		((OmegaDisplayImpl)((PayStationImpl) ps).getDisplayStrategy()).setTime(23, 59);
		ps.addPayment(25);
		ps.addPayment(25); //50 cents
		ps.addPayment(25);
		ps.addPayment(25); //100 cents
		ps.addPayment(25);
		ps.addPayment(25); //150 cents 
		ps.addPayment(5);  //155 cents (155/5 = 31. 31*2 = 62)
		int expectedDisplay = 101;
	    assertEquals( "Should display 101 for 155 cents",
	                  expectedDisplay, ps.readDisplay() );//Really it displays 100 bc the directions seemed to ask 
		//for an int to be displayed
	 }

}
