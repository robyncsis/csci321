/** State pattern implementation of a subway turnstile.  

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class TurnstileImpl implements Turnstile {
	static Ticket ticket;
  State 
    lockedState = new LockedState(this),
    unlockedState = new UnlockedState(this),
    acceptedState = new AcceptedState(this),
    state = lockedState;
  public void ticket() { state.ticket(); }
  public void pass() { state.pass(); }
  
  public static void main(String[] args) {
    System.out.println( "Demo of turnstile state pattern" );
    Turnstile turnstile = new TurnstileImpl();
    
    ticket = new Ticket(true);
    turnstile.ticket();
    ticket.setTaken(true);
    turnstile.ticket();
    turnstile.pass();
    System.out.println();
    
    ticket = new Ticket(false);
    turnstile.ticket();
    turnstile.pass();
    System.out.println();
    
    ticket = new Ticket(true);
    turnstile.ticket();
    ticket.setTaken(true);
    turnstile.ticket();
    turnstile.ticket();
    System.out.println();
    
    ticket = new Ticket(true);
    turnstile.ticket();
    turnstile.pass();
    System.out.println();
    
    ticket = new Ticket(true);
    turnstile.ticket();
    turnstile.ticket();
  }
}

abstract class State implements Turnstile {
  protected TurnstileImpl turnstile;
  public State(TurnstileImpl ts) { turnstile = ts; }
}

class LockedState extends State {
  public LockedState(TurnstileImpl ts) { super(ts); }
  public void ticket() { 
	if(turnstile.ticket.isGood()) {
		System.out.println( "Locked state: Ticket is good. Ticket accepted. RETURN TICKET. ");
    	turnstile.state = turnstile.acceptedState;
    	}
	else {
		System.out.println( "Locked state: Ticket is bad. RETURN TICKET");
	}
  }
  public void pass() { 
    System.out.println( "Locked state: Passenger pass: SOUND ALARM");
  } 
}

class UnlockedState extends State {
  public UnlockedState(TurnstileImpl ts) { super(ts); }
  public void ticket() { 
    System.out.println( "Unlocked state: Ticket entered, REJECT TICKET");
    turnstile.state = turnstile.lockedState;
  }
  public void pass() { 
    System.out.println( "Unlocked state: Passenger pass. LOCK ARMS");
    turnstile.state = turnstile.lockedState;
  }
}

class AcceptedState extends State {
	public AcceptedState(TurnstileImpl ts) { super(ts); }
	public void ticket() { 
		if(turnstile.ticket.isTaken()) {
			System.out.println( "Accepted state: Passenger took ticket. UNLOCK ARMS");
			turnstile.state = turnstile.unlockedState;
		}else {
			System.out.println("Accepted state: Ticket entered, REJECT TICKET");
		}
	}
	public void pass() { 
	  System.out.println( "Accepted state: Passenger pass: SOUND ALARM");
	  turnstile.state = turnstile.lockedState;
	}
}
class Ticket{
	private boolean good;
	private boolean taken;
	public Ticket(boolean good) {this.good = good;}
	
	public boolean isGood() {return good;}
	public boolean isTaken() {return taken;}
	public void setTaken(boolean taken) {this.taken = taken;}
}



