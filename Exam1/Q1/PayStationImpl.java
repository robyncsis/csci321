import java.util.HashMap;
import java.util.Map;

/** Implementation of the pay station.

   Responsibilities:
			
   1) Accept payment;
   2) Calculate parking time based on payment;
   3) Know earning, parking time bought;
   4) Issue receipts;
   5) Handle buy and cancel events.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/

public class PayStationImpl implements PayStation {
  private int insertedSoFar;
  private int timeBought;
  private Map<Integer, Integer> coins;
  
  public PayStationImpl() {
	  insertedSoFar = 0;
	  timeBought = 0;
	  coins = new HashMap<Integer, Integer>();
  }
  
  public void addPayment( int coinValue ) 
          throws IllegalCoinException {
    switch ( coinValue ) {
    case 5: break;
    case 10: break;  
    case 25: break;  
    default: 
      throw new IllegalCoinException("Invalid coin: "+coinValue);
    }
    insertedSoFar += coinValue;
    timeBought = insertedSoFar / 5 * 2;
//    coins = new HashMap<Integer, Integer>();
    addToCoins(coinValue);
  }
  
  private void addToCoins(int coinValue) {
	  if(coins.get(coinValue)==null) {
		  coins.put(coinValue, 1);
	  }else {
		  int numOfCoins = coins.get(coinValue) +1;
		  coins.put(coinValue, numOfCoins);
	  }
		  
  }
  
  public int readDisplay() {
    return timeBought;
  }
  
  public Map<Integer, Integer> getCoins(){
	  return coins;
  }
  public Receipt buy() {
    Receipt r = new ReceiptImpl(timeBought);
    reset();
    return r;
  }
  public Map<Integer, Integer> cancel() {
	Map<Integer, Integer> coins = this.coins;
    reset();
    return coins;
  }
  private void reset() {
    timeBought = insertedSoFar = 0;
    coins = new HashMap<Integer, Integer>();
  }
  

}

