package sevensegmentsrc.domain;

import org.junit.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class NumberDisplayTest {
	
	SevenSegment segment;
	NumberDisplay numDisplay;
	
	@Before
	public void setUp() {
		segment = new SevenSegmentStub();
		numDisplay = new NumberDisplayImpl(segment);
	}

	@Test
	public void testDisplay0() {
		numDisplay.display(0);
		boolean[] bools = new boolean[] {true, true, true, false, true, true, true};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}
	
	@Test
	public void testDisplay1() {
		numDisplay.display(1);
		boolean[] bools = new boolean[] {false, false, true, false, false, true, true};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}
	
	@Test
	public void testDisplay2() {
		numDisplay.display(2);
		boolean[] bools = new boolean[] {true, false, true, true, true, false, true};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}
	
	@Test
	public void testDisplay3() {
		numDisplay.display(3);
		boolean[] bools = new boolean[] {true, false, true, true, false, true, true};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}
	
	@Test
	public void testDisplay4() {
		numDisplay.display(4);
		boolean[] bools = new boolean[] {false, true, true, true, false, true, false};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}
	
	@Test
	public void testDisplay5() {
		numDisplay.display(5);
		boolean[] bools = new boolean[] {true, true, false, true, false, true, true};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}
	
	@Test
	public void testDisplay6() {
		numDisplay.display(6);
		boolean[] bools = new boolean[] {true, true, false, true, true, true, true};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}

	@Test
	public void testDisplay7() {
		numDisplay.display(7);
		boolean[] bools = new boolean[] {true, false, true, false, false, true, false};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}
	
	@Test
	public void testDisplay8() {
		numDisplay.display(8);
		boolean[] bools = new boolean[] {true, true, true, true, true, true, true};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}

	@Test
	public void testDisplay9() {
		numDisplay.display(9);
		boolean[] bools = new boolean[] {true, true, true, true, false, true, false};
		for(int i = 0; i<7; i++) {
			assertEquals(bools[i], ((SevenSegmentStub) segment).getLEDs()[i]);
		}
	}

}
