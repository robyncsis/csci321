package sevensegmentsrc.domain;


public class SevenSegmentStub implements SevenSegment {
	private boolean[] LEDs;
	
	public SevenSegmentStub() {
		LEDs = new boolean[7];
	}
	@Override
	public void setLED(int led, boolean on) {
		LEDs[led] = on;
	}
	
	public boolean[] getLEDs() {
		return LEDs;
	}

}
