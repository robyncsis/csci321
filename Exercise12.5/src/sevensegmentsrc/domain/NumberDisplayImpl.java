package sevensegmentsrc.domain;

public class NumberDisplayImpl implements NumberDisplay {
	private SevenSegment segment;
	private static boolean[] ZERO = new boolean[] {true, true, true, false, true, true, true};
	private static boolean[] ONE = new boolean[] {false, false, true, false, false, true, true};
	private static boolean[] TWO = new boolean[] {true, false, true, true, true, false, true};
	private static boolean[] THREE = new boolean[] {true, false, true, true, false, true, true};
	private static boolean[] FOUR = new boolean[] {false, true, true, true, false, true, false};
	private static boolean[] FIVE = new boolean[] {true, true, false, true, false, true, true};
	private static boolean[] SIX = new boolean[] {true, true, false, true, true, true, true};
	private static boolean[] SEVEN = new boolean[] {true, false, true, false, false, true, false};
	private static boolean[] EIGHT = new boolean[] {true, true, true, true, true, true, true};
	private static boolean[] NINE = new boolean[] {true, true, true, true, false, true, false};
	
	
	public NumberDisplayImpl(SevenSegment segment) {
		this.segment = segment;
	}

	@Override
	public void display(int number) {
		switch(number) {
		case 0:
			processLEDs(ZERO);
			break;
		case 1:
			processLEDs(ONE);
			break;
		case 2:
			processLEDs(TWO);
			break;
		case 3:
			processLEDs(THREE);
			break;
		case 4:
			processLEDs(FOUR);
			break;
		case 5:
			processLEDs(FIVE);
			break;
		case 6:
			processLEDs(SIX);
			break;
		case 7:
			processLEDs(SEVEN);
			break;
		case 8:
			processLEDs(EIGHT);
			break;
		case 9:
			processLEDs(NINE);
			break;
		}
	}
	
	public void processLEDs(boolean[] leds) {
		for(int i = 0 ; i<7 ; i++) {
			segment.setLED(i, leds[i]);
		}
	}

}
