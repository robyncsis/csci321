package paystation.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestDanishPaymentStrategy {
	PayStation ps;
	PaymentStrategy dc;
	
	  /** Fixture for pay station testing. */
	  @Before
	  public void setUp() {
	    ps = new PayStationImpl( new One2OneRateStrategy(), new DanishPaymentStrategy() );
	    dc = new DanishPaymentStrategy();
	  }

	
		/** Test acceptance of all legal coins */
	  @Test
	  public void shouldAcceptLegalCoins() throws IllegalCoinException {
		assertEquals(1, dc.addPayment( 1 ));
		assertEquals(2, dc.addPayment( 2 ));
		assertEquals(5, dc.addPayment( 5 ));
		assertEquals(10, dc.addPayment( 10 ));
		assertEquals(20, dc.addPayment( 20 ));
//	    assertEquals( "Should accept 1, 2, 5, 10, and 20 kroner", 
//	                  1+2+5+10+20, ps.readDisplay() ); 
	  }

	  /** 
	   * Verify that illegal coin values are rejected.
	  */
	  @Test(expected=IllegalCoinException.class)
	  public void shouldRejectIllegalCoin() throws IllegalCoinException {
	    dc.addPayment(17);
	  }

	  /**
	   * Buy should return a valid receipt of the 
	   * proper amount of parking time
	  */
	  @Test 
	  public void shouldReturnCorrectReceiptWhenBuy() 
	    throws IllegalCoinException {
	    ps.addPayment(5);
	    ps.addPayment(10);
	    ps.addPayment(20);
	    Receipt receipt;
	    receipt = ps.buy();
	    assertNotNull( "Receipt reference cannot be null",
	                   receipt );
	    assertEquals( "Receipt value must be correct.",
	                  5+10+20, receipt.value() );
	  }
	  

}
