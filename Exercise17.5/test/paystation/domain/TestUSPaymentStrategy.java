package paystation.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class TestUSPaymentStrategy {
	PayStation ps;
	PaymentStrategy usc;
	
	  /** Fixture for pay station testing. */
	  @Before
	  public void setUp() {
	    ps = new PayStationImpl( new One2OneRateStrategy(), new USPaymentStrategy() );
	    usc = new DanishPaymentStrategy();
	  }

	
		/** Test acceptance of all legal coins */
	  @Test
	  public void shouldAcceptLegalCoins() throws IllegalCoinException {
		assertEquals(5, usc.addPayment( 5 ));
		assertEquals(10, usc.addPayment( 10 ));
		assertEquals(20, usc.addPayment( 20 ));
	  }

	  /** 
	   * Verify that illegal coin values are rejected.
	  */
	  @Test(expected=IllegalCoinException.class)
	  public void shouldRejectIllegalCoin() throws IllegalCoinException {
	    usc.addPayment(17);
	  }

	  /**
	   * Buy should return a valid receipt of the 
	   * proper amount of parking time
	  */
	  @Test 
	  public void shouldReturnCorrectReceiptWhenBuy() 
	    throws IllegalCoinException {
	    ps.addPayment(5);
	    ps.addPayment(10);
	    ps.addPayment(25);
	    Receipt receipt;
	    receipt = ps.buy();
	    assertNotNull( "Receipt reference cannot be null",
	                   receipt );
	    assertEquals( "Receipt value must be correct.",
	                  5+10+25, receipt.value() );
	  }
}
