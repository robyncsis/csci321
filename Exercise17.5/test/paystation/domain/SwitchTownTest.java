package paystation.domain;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SwitchTownTest {
	PayStation ps;
	
//	@Before
//	public void setUp() {
//		ps = new PayStationImpl(new AlphaTownFactory);
//	}

	@Test
	public void switchToBetaTown() throws IllegalCoinException {
		ps = new PayStationImpl(new AlphaTownFactory());
//		Exception exception = assertThrows(NumberFormatException.class, () -> {
//	        Integer.parseInt("1a");
//	    });
		//Assert.assertThrows(IllegalCoinException.class, ps.addPayment(30));
		try {
			ps.addPayment(20);
			assertTrue(false);
		}catch(Exception e){
			assertTrue(true);
		}
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25); 
		ps.addPayment(25);//200 cents
		assertEquals(80, ps.readDisplay()); 
		((PayStationImpl) ps).switchSystem(new BetaTownFactory());
		assertEquals(75, ps.readDisplay()); //Switched to progressive rate strategy
		try {
			ps.addPayment(20);
			assertTrue(false);
		}catch(Exception e){ // Still will not accept Danish currency
			assertTrue(true);
		}
	}

	@Test
	public void switchToAlphaTown() throws IllegalCoinException {
		ps = new PayStationImpl(new BetaTownFactory());
//		Exception exception = assertThrows(NumberFormatException.class, () -> {
//	        Integer.parseInt("1a");
//	    });
		//Assert.assertThrows(IllegalCoinException.class, ps.addPayment(30));
		try {
			ps.addPayment(20);
			assertTrue(false);
		}catch(Exception e){
			assertTrue(true);
		}
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25); 
		ps.addPayment(25);//200 cents
		assertEquals(75, ps.readDisplay()); 
		((PayStationImpl) ps).switchSystem(new AlphaTownFactory());
		assertEquals(80, ps.readDisplay()); //Switched to progressive rate strategy
		try {
			ps.addPayment(20);
			assertTrue(false);
		}catch(Exception e){ // Still will not accept Danish currency
			assertTrue(true);
		}
	}
	
	@Test
	public void switchToDanishTown() throws IllegalCoinException {
		ps = new PayStationImpl(new AlphaTownFactory());
//		Exception exception = assertThrows(NumberFormatException.class, () -> {
//	        Integer.parseInt("1a");
//	    });
		//Assert.assertThrows(IllegalCoinException.class, ps.addPayment(30));
		try {
			ps.addPayment(20);
			assertTrue(false);
		}catch(Exception e){
			assertTrue(true);
		}
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25);
		ps.addPayment(25); 
		ps.addPayment(25);//200 cents
		assertEquals(80, ps.readDisplay()); 
		((PayStationImpl) ps).switchSystem(new DanishTownFactory());
		assertEquals(1400, ps.readDisplay()); //Switched to progressive rate strategy
		try {
			ps.addPayment(25);
			assertTrue(false);
		}catch(Exception e){ // Now will not accept US currency
			assertTrue(true);
		}
	}
	
	@Test
	public void switchBack() throws IllegalCoinException {
		switchToBetaTown();
		((PayStationImpl) ps).switchSystem(new AlphaTownFactory());
		assertEquals(80, ps.readDisplay()); //Switched to progressive rate strategy
		try {
			ps.addPayment(20);
			assertTrue(false);
		}catch(Exception e){ // Still will not accept Danish currency
			assertTrue(true);
		}
	}
}
