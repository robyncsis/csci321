package paystation.domain;

public class DanishTownFactory implements SystemFactory{

	@Override
	public RateStrategy getRateStrategy() {
		return new DanishRateStrategy();
	}

	@Override
	public PaymentStrategy getPaymentStrategy() {
		return new DanishPaymentStrategy();
	}
}
