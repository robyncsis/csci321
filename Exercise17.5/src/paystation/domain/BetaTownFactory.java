package paystation.domain;

public class BetaTownFactory implements SystemFactory{

	@Override
	public RateStrategy getRateStrategy() {
		return new ProgressiveRateStrategy();
	}

	@Override
	public PaymentStrategy getPaymentStrategy() {
		return new USPaymentStrategy();
	}
}
