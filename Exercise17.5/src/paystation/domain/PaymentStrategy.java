package paystation.domain;

public interface PaymentStrategy {
	public int addPayment( int coinValue ) throws IllegalCoinException ;
}
