package paystation.domain;

public class AlphaTownFactory implements SystemFactory{

	@Override
	public RateStrategy getRateStrategy() {
		return new LinearRateStrategy();
	}

	@Override
	public PaymentStrategy getPaymentStrategy() {
		return new USPaymentStrategy();
	}

}
