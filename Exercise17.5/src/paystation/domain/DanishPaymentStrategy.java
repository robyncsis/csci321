package paystation.domain;

public class DanishPaymentStrategy implements PaymentStrategy{

	@Override
	public int addPayment(int coinValue) throws IllegalCoinException {
		switch ( coinValue ) {
	    case 1: break;
	    case 2: break;
	    case 5: break;
	    case 10: break;  
	    case 20: break;  
	    default: 
	      throw new IllegalCoinException("Invalid coin: "+coinValue);
	    }
	    return coinValue;
	}
	
}
