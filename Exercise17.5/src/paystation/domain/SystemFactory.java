package paystation.domain;

public interface SystemFactory {

	public RateStrategy getRateStrategy();
	
	public PaymentStrategy getPaymentStrategy();
}
