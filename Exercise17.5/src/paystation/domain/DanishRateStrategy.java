package paystation.domain;

public class DanishRateStrategy implements RateStrategy{

	@Override
	public int calculateTime(int amount) {
		return amount*7;
	}

}
