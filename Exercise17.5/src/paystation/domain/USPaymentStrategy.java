package paystation.domain;

public class USPaymentStrategy implements PaymentStrategy{

	@Override
	public int addPayment(int coinValue) throws IllegalCoinException {
		switch ( coinValue ) {
	    case 5: break;
	    case 10: break;  
	    case 25: break;  
	    default: 
	      throw new IllegalCoinException("Invalid coin: "+coinValue);
	    }
	    return coinValue;
	}

}
